(load "aoc.lisp")

(defun extract-tokens (string)
    (cl-ppcre:all-matches-as-strings (cl-ppcre:parse-string "([0-9]+)") string)
)

(defun extract-line (ls)
    (cdr (mapcar #'extract-tokens (cl-ppcre:split "[:|]" ls)))
)

(defun count-matches (ln)
    (let* (
        (winning (first ln))
        (have (second ln))
    )

    (reduce #'+ (mapcar #'(lambda (x) (if (find x winning :test #'string=) 1 0)) have))
    )
)

(defun count-points (matches)
    (floor (expt 2 matches) 2)
)

(defun store-points (num dups lst)
    (if (= num 0) 
        lst
        (cons (+ dups (car lst)) (store-points (- num 1) dups (cdr lst)))
    )
)

(defun store-copies (score lst)
    (store-points score (car lst) (cdr lst))
)

(defun process-1 (nums)
    (reduce #'+ (mapcar #'count-points nums))
)

(defun process-2 (nums)
    (setq copies (mapcar #'(lambda (x) 1) nums))
    (loop for x in nums collect
        (progn
            (setq take (car copies))
            (setq copies (store-copies x copies))
            take
        )
    )
)

(setq matches (mapcar #'count-matches (mapcar #'extract-line (load-input 4))))

(sol (process-1 matches) (reduce #'+ (process-2 matches)))