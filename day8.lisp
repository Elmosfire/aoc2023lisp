(load "aoc.lisp")

(defun preproc ()
    (mapcar #'extract-words (load-input 8))
)

(setq mapping (cddr (preproc)))

(setq instruct (loop for x in (to-char-list (caar (preproc))) collect (if (char= #\L x) :left :right)))

(defun traverse (instruct mapping start)
(setq current start)
(setq index 0)
(setq hist nil)
(progn
(loop while (not (string= current "ZZZ")) collect
    (let* (
        (dir (wdebug :dir (nth (mod index (length instruct)) instruct)))
        (cnode (cdr (assoc current mapping :test #'string=)))
    )
    (progn
    (setq current (wdebug :node (ecase dir
        (:left (first cnode))
        (:right (second cnode))
    ))
    )
    (setq index (+ index 1))
    (push current hist)
    (wdebug :hist hist)
    current
    )
    )
)
)
)

(pdebug instruct)

(pdebug (traverse instruct mapping "11A"))