(load "aoc.lisp")

(defun postfixes (chars)
    "Given a list, give all the slices that include the last element. For example, (1 2 3) will yield ((1 2 3) (2 3) (3))"
    (if (not (null chars)) ; if chars is not empty
        (cons chars (postfixes (cdr chars))) ; copy of the full list, and postfixes of the tail
        nil ; else: empty returns empty
    )
)

(defun charlst-starts-with (charlist prefix)
    "Checks of the first characters of charlist match all the characters in prefix"
    (if (null prefix) t ; if prefix is empty, this is vaciously true.  return true
    (if (null charlist) nil ; if prefixes is not empty and charlist is, return false
    (if (char= (car charlist) (car prefix)) ; if first characters match
        (charlst-starts-with (cdr charlist) (cdr prefix)) ; pop first character from both and try again
        nil ; else, return false
    )
    )
    )
)

(setq lookup1 '(
    ("1" . 1)
    ("2" . 2)
    ("3" . 3)
    ("4" . 4)
    ("5" . 5)
    ("6" . 6)
    ("7" . 7)
    ("8" . 8)
    ("9" . 9)
))

(setq lookup2 (concat-lists '(
    ("one" . 1)
    ("two" . 2)
    ("three" . 3)
    ("four" . 4)
    ("five" . 5)
    ("six" . 6)
    ("seven" . 7)
    ("eight" . 8)
    ("nine" . 9)
) lookup1)
)

(defun lookup (postfix table)
    "Test if the postfix starts with any of the keys in the lookup table. If so, return the value, else return nil"
    (cdr (assoc-if #'(lambda (x) (charlst-starts-with postfix (to-char-list x))) table))
)

(defun extract-using-lookup (line table)
    "Returns all matches for every postfix in 'line' from the lookup table"
    (remove nil (mapcar #'(lambda (x) (lookup x table)) (postfixes (to-char-list line))))
)

(defun first-and-last (s)
    "combines to digits in a single number"
    (if (null s)
        0
        (+ (* (first s) 10) (car (last s)))
    )
)

(defun solve (table)
    (reduce #'+ (mapcar #'first-and-last (mapcar #'(lambda (x) (extract-using-lookup x table)) (load-input 1) )))
)

(sol (solve lookup1) (solve lookup2))