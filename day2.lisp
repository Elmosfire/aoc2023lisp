(load "aoc.lisp")

(defun extract-tokens (string)
    (cl-ppcre:all-matches-as-strings (cl-ppcre:parse-string "([a-zA-Z0-9]+|[,;])") string)
)

(defun tokenise (string-tail)
    (loop for x in string-tail collect 
        (if (string= x ";") :semicolon
        (if (string= x ",") :comma
        (if (string= x "red") :red
        (if (string= x "green") :green
        (if (string= x "blue") :blue
            x
        )
        )
        )
        )
        )
    )
)

(defun split (line)
    (let* (
        (tokens (extract-tokens line))
        (head (cadr tokens))
        (tail (wdebug :tail (cddr tokens)))
    )
    (cons head (make-nested (tokenise tail)))
    )
)

(defun make-nested (lst)
    (if (null lst)
        nil
        (let* (
            (head (car lst))
            (tail (cdr lst))
            (contr (make-nested tail))
            (contr-tail1 (cdr contr))
            (contr-head1 (car contr))
            (contr-tail2 (cdr contr-head1))
            (contr-head2 (car contr-head1))
        )
        
        (if (eq head :semicolon)
            (cons nil contr)
        (if (eq head :comma)
            (cons (cons nil contr-head1) contr-tail1)
            (cons (cons (cons head contr-head2) contr-tail2) contr-tail1)
        ))
        )
    )
)

(defun safe-int-parse (val)
    (if (null val)
        0
        (parse-integer val)
    )
)

(defun extract (lst token)
    (safe-int-parse (car (find-if #'(lambda (x) (eq (second x) token)) lst)))
)

(defun simplify (lst)
    (list (wdebug :red (extract lst :red)) (wdebug :green (extract lst :green)) (wdebug :blue (extract lst :blue)))
)

(defun parse-values (game)
    (cons (safe-int-parse (car game)) (apply #'mapcar (cons #'list (mapcar #'simplify (cdr game)))))
)   

(defun max-game (game)
    (cons (car game) (mapcar #'(lambda (x) (apply #'max x)) (cdr game)))
)

(defun max-game (game)
    (cons (car game) (mapcar #'(lambda (x) (apply #'max x)) (cdr game)))
)

(defun is-valid (game)
    (and (<= (second game) 12) (<= (third game) 13) (<= (fourth game) 14))
)

(setq mxgames (mapcar #'max-game (mapcar #'parse-values (mapcar #'split (load-input 2)))))

(defun power (game)
    (* (second game) (third game) (fourth game))
)

(pdebug mxgames)
(sol (reduce #'+ (mapcar #'car (remove-if-not #'is-valid mxgames))) (reduce #'+ (mapcar #'power mxgames)))