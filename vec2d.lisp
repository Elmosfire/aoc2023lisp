(load "aoc.lisp")

;; Vectors

(defstruct vec2d
    x
    y
)

(defun % (x y)
    (make-vec2d :x x :y y)
)

(defun %x (vec2d)
    (vec2d-x vec2d)
)

(defun %y (vec2d)
    (vec2d-y vec2d)
)

(defun %hash (vec2d)
    (+ (* (%x vec2d) 10000) (%y vec2d))
)

(defun %snake (snake)
    (if (< (length snake) 2 )
        nil
        (progn
        (let (
            (x (car snake))
            (y (cadr snake))
        )
            (cons (% x y) (%snake (cddr snake)))
        )
        )
    )
)

(defun %from-string (string)
    (%snake (extract-numbers string))
)

(defun %+ (vec1 vec2)
    (% (+ (%x vec1) (%x vec2)) (+ (%y vec1) (%y vec2)))
)

(defun %= (vec1 vec2)
    (and (= (%x vec1) (%x vec2)) (= (%y vec1) (%y vec2)))
)

(defun %manhattan (vec1 vec2)
    (+ (abs (- (%x vec1) (%x vec2))) (abs (- (%y vec1) (%y vec2))))
)