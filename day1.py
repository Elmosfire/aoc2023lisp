lookup1 = {str(i): i for i in range(10)}

lookup2 = dict(
    one=1,
    two=2,
    three=3,
    four=4,
    five=5,
    six=6,
    seven=7,
    eight=8,
    nine=9
) | lookup1

def translate(line, lookup):
  for index in range(len(line)+1):
    postfix = line[index:]
    for k,v in lookup.items():
      if postfix.startswith(k):
        yield v
        break

def process_line(line, lookup):
  digits = list(translate(line, lookup))
  return 10*digits[0] + digits[-1]

with open("input1.txt") as file:
  print(sum(process_line(line, lookup1) for line in file))
  file.seek(0)
  print(sum(process_line(line, lookup2) for line in file))