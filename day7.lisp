(load "aoc.lisp")

(defun preproc-line (line)
    (let ((
        chunk (cl-ppcre:split " " line)
    ))
    (cons (to-char-list (car chunk)) (parse-integer (cadr chunk)))
    )
)

(setq card-value-alist1
    (cons (cons #\J 11)
    (concat-lists (
        loop for x from 2 to 9 collect
            (cons (car (to-char-list (format nil "~a" x))) x)
    )
    (list
        (cons #\T 10)
        (cons #\Q 12)
        (cons #\K 13)
        (cons #\A 14)
    )
    )
    )
)

(setq card-value-alist2
    (cons 
    (cons #\J 1) 
    (cdr card-value-alist1)
    )
)

(pdebug card-value-alist1)
(pdebug card-value-alist2)

(defun starts-with (lst prefix)
    "Checks of the first characters of charlist match all the characters in prefix"
    (if (null prefix) t ; if prefix is empty, this is vaciously true.  return true
    (if (null lst) nil ; if prefixes is not empty and charlist is, return false
    (if (= (car lst) (car prefix)) ; if first characters match
        (starts-with (cdr lst) (cdr prefix)) ; pop first character from both and try again
        nil ; else, return false
    )
    )
    )
)

(defun sort-counts (counts)
    (sort counts #'(lambda (x y) (> x y)))
)

(defun count-repl (hand)
    (loop for x in (mapcar #'car card-value-alist1) collect
        (count-if (lambda (y) (char= x y)) hand)
    )
)

(defun count-repl1 (hand)
    (sort-counts (count-repl hand))
)

(defun count-repl2 (hand)
    (let* (
        (repl (count-repl hand))
        (sorted-no-J (sort-counts (cdr repl)))
    )
        (cons (+ (car repl) (car sorted-no-j)) (cdr sorted-no-J))
    )   
)

(defun count-repl-gen (hand isp2)
    (if isp2
        (count-repl2 hand)
        (count-repl1 hand)
    )
)

(defun calculate-label-value (hand isp2)
    (cdr (assoc (count-repl-gen hand isp2)
    '(((5) . 6) ((4 1) . 5) ((3 2) . 4) ((3 1 1) . 3) ((2 2 1) . 2) ((2 1 1 1) . 1) ((1 1 1 1 1) . 0))
    :test #'starts-with
    ))
)

(defun hand-value (hand mapping)
    (if (null hand) 0
        (+ (cdr (assoc (car hand) mapping :test #'char=)) (* 100 (hand-value (cdr hand) mapping)))
    )
)

(defun full-hand-value (hand isp2)
    (+ (* 10000000000 (calculate-label-value hand isp2)) (hand-value (reverse hand) (if isp2 card-value-alist2 card-value-alist1)))
)

(defun card-key-value1 (hand)
    (cons (full-hand-value (car hand) nil) (cdr hand))
)

(defun card-key-value2 (hand)
    (cons (full-hand-value (car hand) t) (cdr hand))
)

(setq hands (mapcar #'preproc-line (load-input 7)))

(setq ordered-bids1 (mapcar #'cdr (sort (mapcar #'card-key-value1 hands) #'(lambda (x y) (< (car x) (car y))) )))
(setq ordered-bids2 (mapcar #'cdr (sort (mapcar #'card-key-value2 hands) #'(lambda (x y) (< (car x) (car y))) )))

(pdebug (reduce #'+ (loop for x in (enumerate-alist (cons 0 ordered-bids1)) collect (* (car x) (cdr x)))))
(pdebug (reduce #'+ (loop for x in (enumerate-alist (cons 0 ordered-bids2)) collect (* (car x) (cdr x)))))
