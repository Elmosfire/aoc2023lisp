(load "aoc.lisp")

(defun to-dots (lst)
    (loop for x in lst collect #\.)
)

(defun preproc-gears-helper (lst index)
    (loop for x in (enumerate-alist lst) collect
        (if (char= (cdr x) #\*)
            (list :gear (+ (* index 1000) (car x)))
            (cdr x)
        )
    )
)

(defun preproc-gears (lst)
    (loop for x in (enumerate-alist lst) collect
        (preproc-gears-helper (cdr x) (car x))
    )
)


(defun preproc-input-ud (input)
    (let* (
        (dots (to-dots (car input)))
    )
    (reverse (cons dots (reverse (cons dots input))))
    )
)

(defun preproc-input-lr (input)
    (mapcar #'(lambda (x) (reverse (cons #\. (cons #\. (reverse (cons #\. x)))))) input)
)

(defun extract-line-trios (lst)
    (if (null (third lst))
        nil
        (cons (list (first lst) (second lst) (third lst)) (extract-line-trios (cdr lst)))
    )
)

(defun is-empty-char (char)
    (if (typep char 'character) (char= char #\.) nil)
)

(defun is-empty-row (n)
    (and (is-empty-char (first (second n))) (is-empty-char (second (second n))) (is-empty-char (third (second n))))
)

(defun boxes ()
    (mapcar #'(lambda (x) (mapcan #'identity x)) 
        (mapcan #'identity 
            (mapcar #'(lambda (x) (remove-if #'is-empty-row x))
            (loop for triplet in (extract-line-trios (preproc-input-lr (preproc-input-ud (preproc-gears (mapcar #'to-char-list (load-input 3)))))) collect
            (apply #'mapcar (cons #'list (mapcar #'extract-line-trios triplet))))
            )
    )
    )
)

(defun is-symbol (char)
    (null (find char (to-char-list ".0123456789")))
)

(defun extract-number (char)
    (find char (to-char-list "0123456789"))
)

(defun contains-symbol (box)
    (some #'is-symbol box)
)

(defun all-gears (box)
    (remove-if-not #'(lambda (x) (if (typep x 'character) nil (eq (car x) :gear))) box)
)


(defun center-and-symbol (box)
    (list (extract-number (fifth box)) (contains-symbol box) (all-gears box))
)


(defun split (cas-list)
    (if (null cas-list) nil
    (let* (
        (tail (split (cdr cas-list)))
        (current (car tail))
        (prev (cdr tail))
        (elem (car cas-list))
    )
    (if (null (car elem))
        (if (null current) tail (cons nil tail))
        (cons (cons elem current) prev)
    )
    )
    )
)


(defun parse-chararray-to-int (array)
    (if (null array)
        0
        (+ (char-index #\0 (car array)) (* 10 (parse-chararray-to-int (cdr array))))
    )
)

(defun parse-chararray-to-int-r (array)
    (parse-chararray-to-int (reverse array))
)

(defun extract-number-cond (number)
    (if (some #'second number)
        (parse-chararray-to-int-r  (mapcar #'car number))
        0
    )
)


(defun unique (lst)
    (if (null lst) nil
        (let ((tail (unique (cdr lst))))
        (if (find (car lst) tail) tail (cons (car lst) tail))
        )
    )
)

(defun concat-gears (number)
    (unique (mapcar #'second (reduce #'append (mapcar #'third number))))
)

(defun extract-number-gears (number gearindex)
    (if (find gearindex (concat-gears number))
        (parse-chararray-to-int-r  (mapcar #'car number))
        nil
    )
)

(defun gear-ratio (gear-nums)
    (if (= (length gear-nums) 2)
        (* (first gear-nums) (second gear-nums))
        0
    )
)

(defun gears-count (numbers)
    (loop for gi in (wdebug :gi (unique (mapcan #'concat-gears numbers))) collect
        (gear-ratio (remove-if #'null (loop for part in numbers collect
            (extract-number-gears part gi)
        )
        )
        )
    )
)

(setq data (mapcar #'center-and-symbol (boxes)))
(pdebug (length data))

(setq part-numbers (split data))


(pdebug part-numbers)
(pdebug (mapcar #'extract-number-cond part-numbers))

(sol (reduce #'+ (mapcar #'extract-number-cond part-numbers)) (reduce #'+ (gears-count part-numbers)))