(load "aoc.lisp")
(setf *read-default-float-format* 'double-float)

(setq times1 (extract-numbers (first (load-input 6))))

(setq distance1 (extract-numbers (second (load-input 6))))

(setq data1 (apply #'mapcar (cons #'list (list times1 distance1))))

(setq times2 (extract-numbers (cl-ppcre:regex-replace-all " " (first (load-input 6)) "")))

(setq distance2 (extract-numbers (cl-ppcre:regex-replace-all " " (second (load-input 6)) "")))

(setq data2 (apply #'mapcar (cons #'list (list times2 distance2))))

(defun process (inp)
    (let* (
        (time (first inp))
        (dist (second inp))
        (Discr (sqrt (- (* time time) (* 4 dist))))
        (val (- (ceiling Discr) 1))
    )
    val
    )
)

(pdebug (reduce #'* (mapcar #'process data1)))
(pdebug (reduce #'* (mapcar #'process data2)))