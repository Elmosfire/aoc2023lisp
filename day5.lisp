(load "aoc.lisp")
(ql:quickload "split-sequence")

(setq definitions
    (remove-if #'null (split-sequence:split-sequence nil (mapcar #'extract-numbers (load-input 5))))
)

(defun @@ (start end)
    (list :start start :end end)
)

(defun @@s (start length)
    (@@ start (+ start length))
)

(defun @< (range)
    (getf range :start)
)

(defun @> (range)
    (getf range :end)
)


(defun @in (range value)
    (and (>= value (@< range)) (< value (@> range)))
)


(defun split-range-on-number (range marker)
    (if (@in range marker)
        (list (@@ (@< range) marker) (@@ marker (@> range)))
        (list range)
    )
)

(defun split-ranges-on-number (ranges marker)
    (mapcan #'(lambda (x) (split-range-on-number x marker)) ranges)
)

(defun split-ranges-on-numbers (markers ranges)
    (reduce #'split-ranges-on-number markers :initial-value ranges)
)

(defun rangemap (dest source length)
    (list :range (@@s source length) :shift (- dest source))
)

(defun shift (range amount)
    (@@ (+ (@< range) amount) (+ (@> range) amount))
)

(defun apply-rangemap (rangemap ranges)
    (let* (
        (map-range (getf rangemap :range))
        (map-shift (getf rangemap :shift))
        (preproc-ranges (split-ranges-on-numbers (list (@< map-range) (@> map-range)) ranges))
    )
    (loop for range in preproc-ranges collect
        (if (@in map-range (@< range))
            (list :mapped (shift range map-shift))
            (list :unmapped range)
        )
    )
    )
)

(defun is-mapped (range)
    (eq (car range) :mapped) 
)

(defun is-unmapped (range)
    (eq (car range) :unmapped) 
)

(defun apply-rangemap-to-range-store (rangestore rangemap)
    (let* (
        (ranges-to-process (wdebug :process (getf rangestore :to-process)))
        (ranges-to-keep (wdebug :keep (getf rangestore :to-keep)))
        (results (apply-rangemap (wdebug :map rangemap) ranges-to-process))
        (results-to-process (mapcar #'second (remove-if-not #'is-unmapped results)))
        (results-to-keep (mapcar #'second (remove-if-not #'is-mapped results)))
    )
        (list :to-process results-to-process :to-keep (concat-lists ranges-to-keep results-to-keep))
    )
)

(defun initialise-rangestore (ranges)
    (wdebug :store (list :to-process ranges :to-keep nil))
)

(defun exit-rangestore (rangestore)
    (concat-lists (getf rangestore :to-process) (getf rangestore :to-keep))
)



(defun apply-all-rangemaps (rangemaps initial)
    (exit-rangestore (reduce #'apply-rangemap-to-range-store rangemaps :initial-value (initialise-rangestore initial)))
)

(defun rangemap-from-list (lst)
    (apply #'rangemap lst)
)

(defun apply-from-definition (initial def-element)
    (apply-all-rangemaps (mapcar #'rangemap-from-list (wdebug :apply def-element)) initial)
)

(defun apply-all (initial)
    (reduce #'apply-from-definition (cdr definitions) :initial-value initial)
)

(defun init-line1 (line)
    (if (null line) line
        (cons (@@s (first line) 1) (init-line1 (cdr line)))
    )
)

(defun init-line2 (line)
    (if (null line) line
        (cons (@@s (first line) (second line)) (init-line2 (cddr line)))
    )
)



(sol 
(apply #'min (mapcar #'@< (apply-all (wdebug :init (init-line1 (caar definitions))))))
(apply #'min (mapcar #'@< (apply-all (wdebug :init (init-line2 (caar definitions))))))
)





